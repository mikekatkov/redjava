package Servit1;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.*;


public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws  javax.servlet.ServletException, IOException {
        try(PrintWriter pr = response.getWriter())
        {
            pr.print("Hello");
            Connection conn = null;
            Statement st= null;
            try{
                String sql= "CREATE TABLE IF NOT EXISTS PEOPLE(\n" +
                        "   ID INTEGER PRIMARY KEY  AUTOINCREMENT,\n" +
                        "   NAME           TEXT    NOT NULL,\n" +
                        "   AGE            INT     NOT NULL,\n" +
                        "   ADDRESS        CHAR(50),\n" +
                        "   GENDER         CHAR(1)\n" +
                        ");";
            conn = DriverManager.getConnection("jdbc:sqlite:secondDB");
                st = conn.createStatement();
                st.execute(sql);

            }
            catch(SQLException ex){
                pr.print(ex.getMessage());
            }
            finally {
                try {
                    st.close();
                    conn.close();
                }
                catch (SQLException ex){
                    ex.printStackTrace();
                }
            }
        }
        catch (IOException ex){
            ex.printStackTrace();
        }

    }
}