package Servit1;


import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;
        import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddServelet extends HttpServlet {
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws  javax.servlet.ServletException, IOException
    {
        Connection conn = null;
        PreparedStatement st= null;
        String sql ="INSERT INTO PEOPLE (NAME, AGE, ADDRESS,GENDER)  \n" +
                "VALUES (?, ?, ?, ?);";

        try (PrintWriter pr = response.getWriter())
        {

            try{
            conn= DriverManager.getConnection("jdbc:sqlite:secondDB");
            st = conn.prepareStatement(sql);
            st.setString(1, request.getParameter("name"));
            st.setInt(2, Integer.valueOf(request.getParameter("age")));
            st.setString(3, request.getParameter("address"));
            st.setString(4, request.getParameter("gender"));
            pr.print(st.executeUpdate());
            }
            catch (SQLException ex)
            {
                ex.printStackTrace();
            }
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
