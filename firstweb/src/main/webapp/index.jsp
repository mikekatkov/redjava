<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootst.." />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="container">

    <form action="add" method="POST">
        <div class="form-group">
            <label >Name</label>
            <input type="text"  name="name" class="form-control">
        </div>
        <div class="form-group">
            <label >AGE</label>
            <input type="number" name="age" class="form-control">
        </div>
        <div class="form-group">
            <label>ADDRESS</label>
            <textarea rows="10" name="address" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label >GENDER</label>
            <select name="gender" class=" col-3 form-control">
                <option>M</option>
                <option>F</option>
            </select>
        </div>
        <input class="btn btn-primary" type="submit" value="SAVE" />
    </form>
</div>
</body>
</html>